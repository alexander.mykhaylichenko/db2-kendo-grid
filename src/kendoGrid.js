(function(){

    /*
    
    // the config object stores extra configuration parameters for the table:
    
    extraConfigs = { // example config with all available parameters
        decimals: [ // Array. Each element top->bottom is the number of decimal places for a column in the table left->right respectively
            null, // column 1 is ignored/not rounded
            false, // column 2 is ignored/not rounded
            undefined, // column 3 is ignored/not rounded
            3, // column 4 values must be rounded to 2dp
            true, // column 5 is rounded to default 3dp
            "anything else non-integer but resolved as true" // column 5 is rounded to default
        ]
    }
    
    */
    
    window.loadDB2KendoGrid = function(self, extraConfigs) {
    
        // setup
        // setup Kendo widget registry - registers all Kendo charts to avoid extensibility script re-running when a change is made (workaround DB2)
        //if (!window.kendoWidgetRegistry) window.kendoWidgetRegistry = {};
    
        /*var ID = self.model.get('ID');
        if (!window.kendoWidgetRegistry[ID]) { // if this script not already running
            window.kendoWidgetRegistry[ID] = true; // register self
        } else {
            return; // exit, script for this chart already running
        }*/
    
        var kendoGridConfig = { // Kendo grid UI configuration
            toolbar: ['excel'],
            excel: {
                fileName: self.model.get('Name') + ' ' + (new Date).toLocaleDateString() + '.xlsx'
            },
            theme: 'material',
            sortable: {
                mode: 'single',
                allowUnsort: false
            },
            filter: function() {
                spinner.show();
            },
            sort: function() {
                spinner.show();
            },
            dataBound: function() {
                spinner.hide();
            },
            pageable: {
                pageSizes: [200, 500, 1000, 2000, 20000],
                buttonCount: 5,
                pageSize: 200
            },
            filterable: true,
            scrollable: true,
        };
    
        var filterTemplate = { // default template for all filters required by getData
            ObjectType: '',
            Operator: '',
            Filters: [
                {
                    ID: '',
                    Action: '',
                    Count: '',
                    Description: '',
                    Flags: '',
                    Name: '',
                    ObjectType: '',
                    Operator: '',
                    ParentID: '',
                    ScaleType: '',
                    SourceID: '',
                    Values: [
                        {
                            ObjectType: '',
                            ID: ''
                        }
                    ],
                    Value: ''
                }
            ]
        };
        var chartTemplate = { // template for all charts required by getData
            Name: '',
            ObjectType: '',
            Description: function(description) {
                description = JSON.parse(description);
                description.chartType = 'table';
                return JSON.stringify(description);
    
                //return "{\"DataTileType\":\"table\",\"type\":\"widget\",\"widgetType\":0,\"chartType\":\"table\"}"
            },
            LocalizationKey: '',
            Flags: '',
            GUID: '',
            ID: '',
            SourceID: '',
            Children: [
                {
                    ID: '',
                    Name: '',
                    SourceID: '',
                    ObjectType: '',
                    ParentID: '',
                    ScaleType: '',
                    GUID: '',
                    BeginDate: formatDate, // widget date => getData date needs formatting
                    EndDate: formatDate, // widget date => getData date needs formatting
                    Flags: '',
                    LocalizationKey: '',
                    Filters: [
                        filterTemplate
                    ]
                }
            ],
            Breakdowns: function() { // custom 'Respondents' breakdown - make getData return for a response-level table
                return [
                    {
                        GUID: generateGUID(),
                        Name: 'Respondents',
                        SourceID: self.model.get('Children').first().get('SourceID'),
                        Action: 4,
                        ObjectType: 2,
                        Flags: 1
                    }
                ];
            },
            Filters: function(level) { // filters for whole chart
                var dashboardFilters = getFromTemplate([filterTemplate], self.dataTile.request.get('Filters')); // filters from datatile/outside the chart model
                var chartFilters = getFromTemplate([filterTemplate], level); // filters from chart
        
                $.extend(true, dashboardFilters, chartFilters); // clashing chart filters overwrite dashboard filters
                return dashboardFilters;
            },
            BeginDate: function(date) { // widget date => getData date needs formatting. Use dashboard level date if no chart date set
                return formatDate(date) || formatDate(self.dataTile.request.get('BeginDate'));
            },
            EndDate: function(date) { // widget date => getData date needs formatting. Use dashboard level date if no chart date set
                return formatDate(date) || formatDate(self.dataTile.request.get('EndDate'));
            }
    
        }
    
        var Enums = {
            'Flags': {
                'Undefined':0,
                'Numeric':1,
                'Categorical':2,
                'Debug':4,
                'IsSplit':8,
                'Ascending':16,
                'Descending':32,
                'Deleted':64,
                'NotApplicable':128,
                'NoData':256,
                'Refresh':512,
                'TimeTrend':1024,
                'Sort':2048,
                'Limit':4096,
                'DateRange':8192,
                'StatisticallyHigher':16384,
                'StatisticallyLower':32768,
                'IgnoreAnalystFilters':65536,
                'Shared':131072,
                'Owner':262144,
                'ResponseList':524288,
                'Weighted':1048576,
                'WeighingSchema':2097152,
                'Collapsed':4194304,
                'QuestionGroup':8388608,
                'ManualSort':16777216,
                'IsAdmin':33554432,
                'ApplyAnalystFilters':67108864,
                'IsSecondaryYAxis':134217728,
                'IsSorted':268435456,
                'CustomGranularity':536870912,
                'StatSig':1073741824	
            }
        };
    
        function getFromTemplate(template, level) { // fill the templates with data taken from the chart/model
            if (level === undefined) return;
    
            if (typeof template === 'function') {
                var retVal = template(level);
                if (retVal !== undefined) return retVal;
            } else if (typeof template === 'object' && template.constructor === Object) {
                var returnObj = {};
                Object.keys(template).forEach(function(k) {
                    var retVal = getFromTemplate(template[k], level.get(k));
                    if (retVal !== undefined) returnObj[k] = retVal;
                });
                if (Object.keys(returnObj).length) return returnObj;
            } else if (typeof template === 'object' && template.constructor === Array) {
                var returnArr = level.reduce(function(acc, l) {
                    var retVal = getFromTemplate(template[0], l);
                    return retVal !== undefined? acc.concat([retVal]) : acc;
                }, []);
                if (returnArr.length) return returnArr;
            } else if (template !== '' && template !== undefined) {
                return template;
            } else {
                return level;
            }
        }
    
        function formatDate(date) { // format date for getData call
            if (date && typeof date === 'object' && date.constructor === Date) {
                return [
                    date.getFullYear(),
                    date.getMonth() + 1,
                    date.getDate()
                ].join('-');
            }
    
            return date;
        }
    
        function generateGUID() { // generate GUID the way DB2 does natively
            return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function(e) {
                var t = 16 * Math.random() | 0;
                return ("x" == e ? t : 3 & t | 8).toString(16);
            });
        }
    
        function getData(config) { // getData ajax. Requires config = {request, page, pageSize, 'success' callback, 'error' callback}
            $.ajax({
                type: 'POST',
                url: '/Reporting/GetData',
                dataType: 'json',
                data: [
                    'request=' + JSON.stringify(config.request),
                    'page=' + config.page,
                    'pageSize=' + config.pageSize,
                ].join('&'),
                beforeSend: function(request) {
                    spinner.show();
                    request.setRequestHeader('__RequestVerificationToken', document.querySelector('input[name="__RequestVerificationToken"]').value);
                    request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
                },
                success: function(res) {
                    config.success(res);
                },
                error: function(res) {
                    config.error(res);
                }
            });
        }
    
        function getReport(callback) { // get report/dashboard ajax
            var rvtEl = document.querySelector('input[name="__RequestVerificationToken"]');
            if (!rvtEl || !rvtEl.value) return;
    
            var xhr = new XMLHttpRequest();
            xhr.open('GET', '/Reporting/GetReport?profile=&reportId=' + localStorage['lastReportIdLoaded']);
    
            xhr.setRequestHeader('__RequestVerificationToken', rvtEl.value);
    
            xhr.onreadystatechange = function() {
                if (this.readyState == XMLHttpRequest.DONE && this.status == 200) {
                    callback(JSON.parse(JSON.parse(this.responseText).Value));
                }
            }
    
            xhr.send();
        }
    
        function saveReport(report, callback) { // save report/dashboard ajax
            var rvtEl = document.querySelector('input[name="__RequestVerificationToken"]');
            if (!rvtEl || !rvtEl.value) return;
    
            var xhr = new XMLHttpRequest();
            xhr.open('POST', '/Reporting/SaveReport');
    
            xhr.setRequestHeader('__RequestVerificationToken', rvtEl.value);
            xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
    
            xhr.onreadystatechange = function() {
                if (this.readyState == XMLHttpRequest.DONE && this.status == 200) {
                    callback(this.responseText);
                }
            }
    
            xhr.send('report=' + encodeURIComponent(JSON.stringify(report)) + '&isSnapshot=false&expirationDate=&companyName=&reportType=1');
        }
    
        function getChartFromReport(report, GUID) {
            function flatten(rep) { // flatten the report to expose all charts in a 1D array
                return rep.reduce(function(flat, toFlatten) {
                    if (toFlatten.Children) {
                        if (toFlatten.ObjectType == 9) {
                            return flat.concat(flatten(toFlatten.Children));
                        } else if (toFlatten.ObjectType < 9) {
                            return flat.concat(toFlatten.Children);
                        }
                    }
                    return flat;
                }, []);
            }
    
            var charts = flatten(report.Children);
            return charts.find(function(chart) {
                return chart.GUID === GUID;
            });
        }
    
        //window.self1 = self;
    
        var container = self.$el.find(self._uiBindings.content); // element into which Kendo is inserted
        var spinner = self.dataTile.ui.loading; // default chart spinner
        var firstGetDataCount = 0;
        var currPageSize = 0;
    
        // add scripts and styles before running main
        if (!window.kendo) {
            $('head').append('<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.2.620/styles/kendo.common-material.min.css">') // main Kendo material styles
            $('head').append('<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.2.620/styles/kendo.material.min.css">') // more material styles
            $('head').append('<style>.k-loading-image{display:none!important;}.k-pager-wrap .k-dropdown{width: 6.2em!important}.k-grid-toolbar,.k-button{background-color: #999!important; border-color: #999!important;}.k-state-selected{border-color:#999 transparent transparent!important}.k-filter-menu .k-button{height: 100%!important;color: #fff!important;}</style>') // minor Kendo style tweaks
    
            $.getScript('https://kendo.cdn.telerik.com/2018.2.620/js/kendo.all.min.js').done(function() { // get main Kendo script
                $.getScript('https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.5/jszip.min.js').done(function() { // export to Excel script
                    main(); // start main script
                });
            });
        } else {
            main(); // start main script
        }
    
        function main() { // main Kendo script (after all setup complete)
            // hide default export to excel option
            $('head').append('<style>[chartid="' + self.model.get('ID') + '"] .js-chartExport{display:none!important;}</style>');
    
            var config = getFromTemplate(chartTemplate, self.model);
            //console.log(JSON.stringify(config));
            setColumns(config, function(columns) {
                //console.log(JSON.stringify(config));
    
                kendoGridConfig.columns = columns; // add/modify columns in config
                createKendoGrid(config, columns);
                //console.log(JSON.stringify(config));
            });
    
            function setColumns(config, callback) {
                getData({ // get one row of data to get column names - get name from data if column/field is translated etc
                    request: config,
                    page: Math.floor(Math.random() * 999) + 100,
                    pageSize: Math.floor(Math.random() * 45) + 5, // apparently Platform doesn't always feel like translating if pageSize = 1. Never use 1.
                    success: function(data) {
                        firstGetDataCount = data.C;
    
                        var columns = self.model.get('Children').map(function(child, i) {
                            var colID = 'id' + child.get('ID');
                            var nameOverride = (JSON.parse(child.get('Description'))).attributeNameOverride;
                            var name = nameOverride !== '' && nameOverride !== undefined? nameOverride : data.Ch[i].N; // if field has been manually renamed in chart, use that
                            
                            //console.log(nameOverride);console.log(data.Ch[i].N);
                            //console.log(name);
    
                            return {
                                field: 'id' + child.get('ID'),
                                title: name,
                                sortable: { // universal sorting function for alphanumerics - DEPRECATED
                                    compare: function(a, b) {
                                        return a[colID].toString().localeCompare(b[colID], undefined, {numeric: true});
                                    }
                                }
                                //hidden: /level/i.test(child.get('Name')) ? true : false
                            }
                        });
    
    
                        // make first column sorted if no columns are already sorted
                        if (!self.model.get('Children').find(function(child) {
                            return child.getFlags().includes('Sort');
                        })) {
                            // set first column to sort descending by default
                            self.model.get('Children').first().setFlag(Enums.Flags.Sort);
                            self.model.get('Children').first().setFlag(Enums.Flags.Descending);
                        }
    
                        callback(columns);
                    },
                    error: function(err) {}
                });
            }
    
            function createKendoGrid(config) { // build the Kendo grid with the configuration
                //console.log(config);
                kendoGridConfig.dataSource = { // add/modify data source in config
                    transport: {
                        read: function(options) {
                            currPageSize = options.data.pageSize;
    
                            // modify sort Flag
                            if (options.data.sort && options.data.sort.length) {
                                // unset all sorts first
                                config.Children.forEach(function(c) {
                                    self.model.get('Children').findWhere({ID: c.ID}).unsetFlag(Enums.Flags.Sort);
                                });
    
                                options.data.sort.forEach(function(s) {
    
                                    var ID = s.field.substring(2);
                                    var field = self.model.get('Children').find(function(c) {
                                        //console.log(flag); console.log(ID); console.log(c);
                                        return c.get('ID') == ID;
                                    });
                                    
                                    if (s.dir == 'asc') { // set ascending Flag 2064
                                        field.setFlag(Enums.Flags.Sort);
                                        field.unsetFlag(Enums.Flags.Descending);
                                        field.setFlag(Enums.Flags.Ascending);
                                    } else if (s.dir == 'desc') { // set descending Flag 2080
                                        field.setFlag(Enums.Flags.Sort);
                                        field.unsetFlag(Enums.Flags.Ascending);
                                        field.setFlag(Enums.Flags.Descending);
                                    } else {
                                        field.unsetFlag(Enums.Flags.Sort);
                                        field.unsetFlag(Enums.Flags.Ascending);
                                        field.unsetFlag(Enums.Flags.Descending);
                                    }
                                });
                            }
    
                            config.Children.forEach(function(c) {
                                c.Flags = self.model.get('Children').findWhere({ID: c.ID}).get('Flags'); // add Flags to config
                            });
    
                            getData({
                                request: config,
                                page: options.data.page,
                                // every hour force cache refresh, reset page size every 12 hours
                                pageSize: options.data.pageSize + (new Date().getHours() % 12),
                                success: function(res) {
                                    options.success(res);
                                },
                                error: options.error
                            });
                        }
                    },
                    serverPaging: true,
                    serverSorting: true,
                    schema: {
                        data: function(rawData) { // parse data into Kendo-readable format
                            var data = [];
    
                            //console.log(rawData);
    
                            rawData.Ch.forEach(function(column, colI) {
                                column.Ch.forEach(function(subColumn) {
                                    var colID = 'id' + column.ID;
                                    
                                    if (!subColumn.Vs) return; // no data
    
                                    subColumn.Vs.forEach(function(value, i) {
                                        if (!data[i]) {
                                            data[i] = {};
                                        }
    
                                        // decimal places
                                        var dp = NaN;
                                        if (extraConfigs && extraConfigs.decimals && (extraConfigs.decimals[colI] || parseInt(extraConfigs.decimals[colI]) == 0)) {
                                            dp = parseInt(extraConfigs.decimals[colI]);
                                            if (isNaN(dp)) dp = 3;
                                        }
    
                                        data[i][colID] = value.V === '' ? 'N/A' : (isNaN(dp)? value.V : parseFloat(value.V).toFixed(value.V % 1 && dp || 0)); // if blank, show 'N/A'
                                    });
                                });
                            });
    
                            //console.log('second getData length before trim to ' + currPageSize + ': ' + data.length);
                            return data.slice(0, currPageSize);
                        },
                        total: function(response) { // get total row count
                            return firstGetDataCount > response.C? firstGetDataCount : response.C; //> currPageSize? currPageSize : response.C;
                        }
                    }
                };
    
                // add sort if column already sorted
                var sortedField = self.model.get('Children').find(function(child) {
                    return child.hasFlag(Enums.Flags.Sort);
                });
                if (sortedField) {
                    kendoGridConfig.dataSource.sort = {
                        field: 'id' + sortedField.get('ID'), 
                        dir: sortedField.hasFlag(Enums.Flags.Ascending)? 'asc' : 'desc'
                    };
                }
    
                //console.log('creating');
                container.kendoGrid(kendoGridConfig); // create Kendo grid
    
                // bind to export to excel post-initialization
                container.data('kendoGrid').bind('excelExport', function(e) {
                    spinner.hide();
                });
            }
    
            // listen to chart/widget creation/changes. Rebuild config and get new data every time a change occurs
            self.model.listenTo(self.model, 'change', function(e) {
                //console.log('change');
                //console.log(e);
    
    
                //onchange();
    
                // terminate self - new script generated by DB2 will take over
                //container.empty();
    
                /*var config = getFromTemplate(chartTemplate, self.model);
                console.log(config);
                setColumns(config, function(columns) {
                    kendoGridConfig.columns = columns; // add/modify columns in config
                    console.log(kendoGridConfig);
                    container.empty();
                    createKendoGrid(config, columns);
                });*/
            });
    
            onChange();
            function onChange() {
                // save changes
                getReport(function(report) {
                    var thisChart = getChartFromReport(report, self.model.get('GUID'));
    
                    //console.log(thisChart);
    
                    // change this chart to column-only table with all configs
                    description = JSON.parse(thisChart.Description);
                    if (!thisChart.Breakdowns || description.chartType !== 'table') { // check and/or change chart type
                        //thisChart = getFromTemplate(chartTemplate, self.model);
                        description.chartType = 'table';
                        thisChart.Description = JSON.stringify(description);
                        thisChart.Breakdowns = chartTemplate.Breakdowns();
    
                        //console.log('saving');console.log(thisChart);console.log(report);
    
                        saveReport(report, function(res) {
                            if (confirm('New Kendo Grid chart has been saved. The page must be reloaded to continue. Please click OK reload the page now')) {
                                window.location.reload();
                            } else {
                                console.warn('New Kendo Grid chart has been saved. Please reload the page now');
                            }
                        });
                    }
                });
            }
        }
    
    }
    
    })();