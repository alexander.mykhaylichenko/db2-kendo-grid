// load latest Kendo script
if (!window.loadDB2KendoGrid && !window.gettingKendoGridScript) {
    window.gettingKendoGridScript = true;
    $.getScript('https://platform-services.custom.eu.mcx.cloud/DB2Extensibility/kendoGrid.js', function () {
        delete window.gettingKendoGridScript;
        loadDB2KendoGrid(self, 'response');
    });
} else if (window.gettingKendoGridScript) {
    var wait = setInterval(function() {
        if (window.loadDB2KendoGrid) {
            clearInterval(wait)
            loadDB2KendoGrid(self, 'response');
        }
    }, 500);
} else {
    loadDB2KendoGrid(self, 'response');
}
