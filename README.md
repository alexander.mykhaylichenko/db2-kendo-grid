# DB2 Kendo Grid

DB2 Kendo Grid is an extensible widget for Dashboards 2 allowing a grid (table) to be rendered using the Kendo UI library in the reporting area, Kendo Grids allow extra features and more flexibility than default DB2 tables.

![Snapshot of a Kendo Grid](snapshot.jpg?raw=true "Snapshot, a kendo grid in a dashboard")

**Warning: Dashboard PDF exports or snapshots are not supported**

## Usage

For now, each Kendo Grid has to be created manually using the extensibility widget provided by Dashboards. You must be a super admin to create these.

### Installation

There are no configuration options, simply follow the default extensibility installation steps below.

1. Sign in as the owner of the dashboard you would like to add the Kendo Grid to.
2. Enable the extensibility widget function by adding **?ext=1** into the URL after the domain, for example "...mcxplatform.de/?ext=1/#r/9e9bf3e9...", and hit enter.
3. Now you may drag in an extensibiltiy widget from the sidebar **Widgets > Extensible Content** to the desired position in your report
4. Open the widget's editor.
5. **First**, you need to add the data source, drag in the **Respondents** field from the correct source (survey) in the side panel. Note, you must select from the correct source because it cannot be changed later, you must destroy and rebuild the widget (or make another one) if you want to use a different source later.
6. In the **Text / Image** textbox add a single space.
7. **Lastly**, open the **Javascript** tab and paste in the code from **widget.js** into the textbox.
8. Click Apply and wait. After a few seconds the widget will set itself up and show you an alert asking you to click OK. Click OK and the page will refresh. _if this does not happen see the Troubleshooting section_.
9. Now, if it worked the widget will now show a message like "a problem occured reading the data". Go into the widget's editor again, notice the Javascript tab has now disappeared (you can no longer edit the configuration... well, you can but you will need to edit the page's CSS). You can now remove the Respondents data field (which cannot be used in this type of chart, hence the warning message on the widget) and replace it with the desired verbatim(s), click apply when done. _If when you edit the widget you still see the Javascript tab, see the Troubleshooting section_.
10. Enjoy your new Kendo Grid!

### Features

- All chart-level and dashboard-level filters and date ranges fully supported
- Sorting by Metric/column fully supported
- Export to Excel
- Column-level filters. **Warning these only apply to the current page - the table data is paginated to prevent performance issues**
- Paginated and has a choice of page sizes
- Displays total count (total number of responses/rows)

## Troubleshooting

### On step 8 of the installation process I waited for ages but no alert ever showed up

Sometimes when adding Javascript to an extebsibility widget it does not get saved, this seems to happen mostly when changing all the settings in the widget editor too fast. Just try steps 5 - 8 again, or check all of the things you did in those steps got saved when doing it the first time, if some things are missing just add them in again but with a couple of seconds or so between each step, and a wait a couple of seconds before you hit apply. Also, try to resist changing the name of the widget or making any other change during the installation process, you can change it after the Kendo Grid shows correctly.

### On step 9 of the installation process I still see the Javascript tab

This is also related to the issue mentioned above, where the Javascript in extensibility widgets does not get saved, simply go through steps 5 - 8 again and correct anyy parts that did not get saved and don't change things too quickly.

### The data in the Kendo Grid does not match the data in the response list / standard reports

This is a known bug and difficult to resolve. This is potentially due to a dashboard caching mechanism that cannot be controlled, the script tries its best to force the cache to refresh or not get created at all but sometimes it does not work and gets cached anyway.

### I can't get the Kendo Grid to show in a PDF export or snapshot

Kendo Grids are not currently supported here.